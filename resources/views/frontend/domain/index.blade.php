@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="container">
        <div id="whoisDmn" class="bg-grey-72">
        <div class="container">
            <div class="whoisInner box-wrap" id="resultSearchDmn">
                <h1 class="page-title h1 text-uppercase text-white text-center">Kiểm tra tên miền</h1>
                <form class="domain-search-form " name="register-domain-name" method="get" action="">
                    <div class="input-group input-group-lg">
                        <input type="text" required="" class="form-control input-search" value="{{$get}}" placeholder="Nhập tên miền bạn muốn kiểm tra" name="domain">
                            <span class="input-group-btn">
                                <button type="submit" class="btn bg-yellow-f7c641 btn-search searchAgain" id="show-btn">
                                    Kiểm tra
                                </button>
                            </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if($data['status']==0)
    <div style="width: 110%;">
        <table class="table i-table-st1">
                                    <thead>
                                    <tr>
                                        <th>
                                            Tên miền
                                        </th>
                                        <th>
                                            <a href="#" target="_blank" class="ng-binding">{{$data['domain_name']}}</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            Ngày đăng ký:
                                        </td>
                                        <td>
                                            <span ng-bind="domain.creationDate" class="ng-binding">{{$data['date_created']}}</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Ngày hết hạn:
                                        </td>
                                        <td>
                                            <strong ng-bind="domain.expirationDate" class="ng-binding">{{$data['date_expires']}}</strong>
                                            <span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Cờ trạng thái :</label>
                                        </td>
                                        <td>
                                            <div ng-repeat="st in domain.status" class="ng-scope">
                                                <span class="ng-binding">clientTransferProhibited</span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Quản lý tại Nhà đăng ký:
                                        </td>
                                        <td>
                                            <span ng-bind="domain.registrar" class="ng-binding">{{$data['nameservers']['0']}}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
    </div>
    @endif
    </div>
@endsection
