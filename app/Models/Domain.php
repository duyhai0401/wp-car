<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
    	'supplier_id',
    	'user_id',
    	'name',
    	'end_date',
    	'status',
    	'api_update'
    ];
}
